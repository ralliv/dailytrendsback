# README #

-. Project DailyTrends // Backend .-

### Quick Intro ###

- DailyTrends is an app that shows news extracted fron de front pages of the main newspapers in Spain.
- This project is divided into two parts. In the current repository, there is the Backend part of the app.
- v 1.0.0


### Setup ###

* Versions and dependencies
  - Nodejs: 8.11.1 LTS
  - MongoDB: 3.6.4
  - Mongoose: 5.0.17
  - Express: 4.16.3

* Configuration
* Dependencies
* Database configuration
* Tests
* Deployment instructions


### Contact ###

- villar.adrian.93@gmail.com